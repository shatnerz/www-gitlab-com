---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-01-31.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 224   | 100%        |
| Based in the US                           | 121   | 54.02%      |
| Based in the UK                           | 17    | 7.59%       |
| Based in the Netherlands                  | 10    | 4.46%       |
| Based in Other Countries                  | 76    | 33.93%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 224   | 100%        |
| Men                                       | 183   | 81.70%      |
| Women                                     | 41    | 18.30%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 25    | 100%        |
| Men in Leadership                         | 21    | 84.00%      |
| Women in Leadership                       | 4     | 16.00%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 103   | 100%        |
| Men in Development                        | 92    | 89.32%      |
| Women in Development                      | 11    | 10.68%      |
| Other Gender Options                      | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 121   | 100%        |
| Asian                                     | 10    | 8.26%       |
| Black or African American                 | 2     | 1.65%       |
| Hispanic or Latino                        | 6     | 4.96%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.83%       |
| Two or More Races                         | 1     | 0.83%       |
| White                                     | 68    | 56.20%      |
| Unreported                                | 33    | 27.27%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 25    | 100%        |
| Asian                                     | 4     | 16.00%      |
| Black or African American                 | 1     | 4.00%       |
| White                                     | 14    | 56.00%      |
| Unreported                                | 6     | 24.00%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 21    | 100%        |
| Asian                                     | 2     | 9.52%       |
| Native Hawaiian or Other Pacific Islander | 1     | 4.76%       |
| White                                     | 11    | 52.38%      |
| Unreported                                | 7     | 33.33%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 224   | 100%        |
| Asian                                     | 14    | 6.25%       |
| Black or African American                 | 5     | 2.23%       |
| Hispanic or Latino                        | 12    | 5.36%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.45%       |
| Two or More Races                         | 2     | 0.89%       |
| White                                     | 115   | 51.34%      |
| Unreported                                | 75    | 33.48%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 103   | 100%        |
| Asian                                     | 6     | 5.83%       |
| Black or African American                 | 3     | 2.91%       |
| Hispanic or Latino                        | 5     | 4.85%       |
| Two or More Races                         | 1     | 0.97%       |
| White                                     | 50    | 48.54%      |
| Unreported                                | 38    | 36.89%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 25    | 100%        |
| Asian                                     | 2     | 8.00%       |
| Native Hawaiian or Other Pacific Islander | 1     | 4.00%       |
| White                                     | 12    | 48.00%      |
| Unreported                                | 10    | 40.00%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 224   | 100%        |
| 20-24                                     | 14    | 6.25%       |
| 25-29                                     | 52    | 23.21%      |
| 30-34                                     | 61    | 27.23%      |
| 35-39                                     | 35    | 15.63%      |
| 40-49                                     | 41    | 18.30%      |
| 50-59                                     | 18    | 8.04%       |
| 60+                                       | 1     | 0.45%       |
| Unreported                                | 2     | 0.89%       |

